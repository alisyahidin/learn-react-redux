import React from 'react';

const HelloWorld = (props) => {
    return(
      <h1>Hello {props.tech}</h1>
    )
}

export default HelloWorld;