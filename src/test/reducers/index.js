export default (state, action) => {
  switch (action.type) {
    case "SET_TECHNOLOGY":
    // state.tech = action.tech
      console.log({...state, tech: action.tech})
      return {
        ...state,
        tech: action.tech
      };

    default:
      return state;
  }
};