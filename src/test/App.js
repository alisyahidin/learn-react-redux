import React, { Component } from "react";
import HelloWorld from "./HelloWorld";
import ButtonGroup from "./ButtonGroup";
import store from './stores';

class App extends Component {
  render() {
    return (
      <div>
        <HelloWorld tech={store.getState().tech} />
        <ButtonGroup technologies={["React", "Elm", "React-redux"]} />
      </div>
    );
  }
}

export default App;