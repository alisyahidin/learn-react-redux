import React from 'react'
import ReactDOM from 'react-dom'
// import { Provider } from 'react-redux'
// import { createStore } from 'redux'
// import todoApp from './reducers'
// import App from './components/App'
import App from './test/App';
// import { devToolsEnhancer } from 'redux-devtools-extension';
import registerServiceWorker from './registerServiceWorker';

import store from './test/stores';
 
const render = () => ReactDOM.render(<App />, document.getElementById("root"));

render()

store.subscribe(render);

registerServiceWorker();
